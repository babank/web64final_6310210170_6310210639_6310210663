import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from "react-router-dom";

import { createTheme, ThemeProvider } from '@mui/material/styles';
import { green, purple } from '@mui/material/colors';
<style>
@import url('https://fonts.googleapis.com/css2?family=Sriracha&display=swap');
</style>

const theme = createTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#ff9800',
    },
    secondary: {
      main: '#f50057',
    },
    background: {
      paper: 'rgba(230,127,0,0.39)',
    },
  },
});

ReactDOM.render(
  <BrowserRouter>
    <ThemeProvider theme={theme}>
    <App />
    </ThemeProvider>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
