import logo from './logo.svg';
import './App.css';

import Header from './components/header';
import Footer from './components/footer';
import Home from './components/home';
import Product from './components/Product';
import Buy from './components/Buy';
import Finish from './components/Finish';
import Shoes from './components/Shoes';
import Tshirt from './components/Tshirt';
import Cap from './components/Cap';
import Pant from './components/Pant';
import Bag from './components/Bag';
import Login from './components/Login';
import Signup from './components/Signup';

import { Routes, Route, Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={
          <Home />
        } />
        <Route path="/home" element={
          <Home />
        } />
        <Route path="/product" element={
          <Product />
        } />
        <Route path="/buy" element={
          <Buy />
        } />
        <Route path="/finish" element={
          <Finish />
        } />
        <Route path="/shoes" element={
          <Shoes />
        } />
        <Route path="/tshirt" element={
          <Tshirt />
        } />
        <Route path="/cap" element={
          <Cap />
        } />
        <Route path="/pant" element={
          <Pant />
        } />
        <Route path="/bag" element={
          <Bag />
        } />
        <Route path="/signup" element={
          <Signup />
        } />
        <Route path="/login" element={
          <Login />
        } />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
