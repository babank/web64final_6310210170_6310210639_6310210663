import vintage from './images/vintage.png'
import vintage1 from './images/vintage1.png'
import vintage2 from './images/vintage2.png'
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Home() {

    return (
        <div>
            <div className="home" style={{ backgroundColor: '#F9E8CD' }}>
                <br />
                <div>
                    <Box sx={{ mx: 70, my: 1 }}>
                        <Paper elevation={20} >
                            <br />
                            <h1> &nbsp;&nbsp;ใส่ใจงานผลิตทุกขั้นตอน การันตีคุณภาพ </h1>
                            <h1> &nbsp;&nbsp;สินค้าได้มาตรฐาน งานสกรีนสวยงามคงทน </h1>
                            <h1> &nbsp;&nbsp;งานดีการันตีคุณภาพ บริการลูกค้าด้วยใจ </h1>
                            <br />
                        </Paper>
                    </Box>
                </div>
                <img src={vintage} style={{ width: "400px" }} />
                <img src={vintage1} style={{ width: "400px" }} />
                <img src={vintage2} style={{ width: "400px" }} />
            </div>
        </div>
    );
}

export default Home;

