import vintage4 from './images/vintage4.png'
import vintage5 from './images/vintage5.png'
import vintage6 from './images/vintage6.png'
import vintage7 from './images/vintage7.png'
import vintage8 from './images/vintage8.png'

import Button from '@mui/material/Button'
import './Product.css';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Product() {

        return (
                <div className="products" style={{ backgroundColor: '#F9E8CD' }}>
                        <br />
                        <div>
                                <Box sx={{ mx: 90, my: 1 }}>
                                        <Paper elevation={20} >
                                                <h1>ชนิดสินค้า</h1>
                                        </Paper>
                                </Box>
                        </div>
                        <div className="tshirt">
                                <center>
                                        <img src={vintage4} style={{ width: "400px" }} />
                                </center>
                                <Button variant="contained" href="/tshirt" size="large" style={{ marginTop: "-50px", fontWeight: "bold" }}> เสื้อวินเทจ </Button>
                        </div>

                        <div className="shoes">
                                <center>
                                        <img src={vintage5} style={{ width: "400px" }} />
                                </center>
                                <Button variant="contained" href="/shoes" size="large" style={{ marginTop: "-50px", fontWeight: "bold" }}> รองเท้าวินเทจ </Button>
                        </div>

                        <div className="bag">
                                <center>
                                        <img src={vintage6} style={{ width: "400px" }} />
                                </center>
                                <Button variant="contained" href="/bag" size="large" style={{ marginTop: "-50px", fontWeight: "bold" }}> กระเป๋าวินเทจ </Button>
                        </div>

                        <div className="cap">
                                <center>
                                        <img src={vintage7} style={{ width: "400px" }} />
                                </center>
                                <Button variant="contained" href="/cap" size="large" style={{ marginTop: "-50px", fontWeight: "bold" }}> หมวกวินเทจ </Button>
                        </div>

                        <div className="pant">
                                <center>
                                        <img src={vintage8} style={{ width: "400px" }} />
                                </center>
                                <Button variant="contained" href="/pant" size="large" style={{ marginTop: "-50px", fontWeight: "bold" }}> กางเกงวินเทจ </Button>
                        </div>


                </div>
        );
}

export default Product;
