import Button from '@mui/material/Button'
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

import * as React from 'react';

import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';


function Buy() {
    const [age, setAge, size] = React.useState('');

    const handleChange = (event) => {
        setAge(event.target.value)
    };

    return (
        <div>
            <div className="Buy" style={{ backgroundColor: '#F9E8CD' }}>
                <br />

                <div>
                    <Box sx={{ mx: 90, my: 1 }}>
                        <Paper elevation={20} >
                            <h1>การสั่งจอง</h1>
                        </Paper>
                    </Box>
                </div>

                <div>
                    <div>
                        <Box sx={{ mx: 80, my: 1 }}>
                            <Paper elevation={20} >
                                <br />
                                <FormControl style={{ marginBottom: 20, width: 135 }}>
                                    <InputLabel id="demo-simple-select-label">ชนิดสินค้า</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={age}
                                        label="Age"
                                        onChange={handleChange}>
                                        <MenuItem value={10}>รองเท้าวินเทจ</MenuItem>
                                        <MenuItem value={20}>เสื้อวินเทจ</MenuItem>
                                        <MenuItem value={30}>หมวกวินเทจ</MenuItem>
                                        <MenuItem value={40}>กางเกงวินเทจ</MenuItem>
                                        <MenuItem value={50}>กระเป๋าวินเทจ</MenuItem>
                                    </Select>
                                </FormControl>

                                <Box
                                    component="form"
                                    sx={{
                                        '& > :not(style)': { m: 1, width: '25ch' },
                                    }}
                                    noValidate
                                    autoComplete="off">
                                    <TextField id="outlined-basic" label="ชื่อสินค้า" variant="outlined" />
                                </Box>

                                <br />

                                <FormControl style={{ marginBottom: 20, width: 135 }}>
                                    <InputLabel id="demo-simple-select-label">ขนาด</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={size}
                                        label="Size"
                                        onChange={handleChange}>
                                        <MenuItem value={11}>S</MenuItem>
                                        <MenuItem value={12}>M</MenuItem>
                                        <MenuItem value={13}>L</MenuItem>
                                        <MenuItem value={14}>XL</MenuItem>
                                    </Select>
                                </FormControl>

                                <Box
                                    component="form"
                                    sx={{
                                        '& > :not(style)': { m: 1, width: '25ch' },
                                    }}
                                    noValidate
                                    autoComplete="off">
                                    <TextField id="outlined-basic" label="จำนวน" variant="outlined" />
                                </Box>

                                <br />

                                <Box
                                    component="form"
                                    sx={{
                                        '& > :not(style)': { m: 1, width: '25ch' },
                                    }}
                                    noValidate
                                    autoComplete="off">
                                    <TextField id="outlined-basic" label="ราคา" variant="outlined" />
                                </Box>
                                <br />
                            </Paper>
                        </Box>
                    </div>

                    <br />

                    <div>
                        <Box sx={{ mx: 90, my: 1 }}>
                            <Paper elevation={20} >
                                <h2 style={{ marginTop: "10px" }}>กรอกที่อยู่จัดสั่ง</h2>
                            </Paper>
                        </Box>
                    </div>

                    <div>
                        <Box sx={{ mx: 50, my: 1 }}>
                            <Paper elevation={20} >
                                <br />
                                ชื่อ : <input type="text" />&nbsp;&nbsp;
                                นามสกุล : <input type="text" /><br /><br />
                                &nbsp;&nbsp;ที่อยู่ : <input type="text" />
                                &nbsp;&nbsp;หมู่ที่ : <input type="text" />
                                &nbsp;&nbsp;แขวน/ตำบล : <input type="text" />
                                &nbsp;&nbsp;เขต/อำเภอ : <input type="text" /><br /><br />
                                &nbsp;&nbsp;จังหวัด : <input type="text" />
                                &nbsp;&nbsp;รหัสไปรษณีย์ : <input type="text" />
                                &nbsp;&nbsp;ประเทศ : <input type="text" /><br /><br />
                                เบอร์โทรศัพท์ : <input type="text" /><br /><br />
                            </Paper>
                        </Box>
                        <br /><br />
                    </div>

                    <Button variant="contained" href="/home" size="large"
                        style={{ marginTop: "-1px", fontWeight: "bold" }}> ยกเลิก </Button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Button variant="contained" href="/finish" size="large"
                        style={{ marginTop: "-1px", fontWeight: "bold" }}> ตกลง </Button>
                    <br /><br /><br />
                </div>

            </div>
        </div>
    );
}

export default Buy;
