import cap11 from './images/cap11.png'
import cap12 from './images/cap12.png'
import cap13 from './images/cap13.png'
import cap14 from './images/cap14.png'


import Button from '@mui/material/Button'
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Cap() {
    return (
        <div style={{ backgroundColor: '#F9E8CD' }}>
            <br />
            <Box sx={{ mx: 100, my: 0 }}>
                <Paper elevation={20} >
                    <h1 style={{ marginTop: "10px" }}>หมวกวินเทจ</h1>
                </Paper>
            </Box>

            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={cap11} style={{ width: "300px", marginBottom : -80 }} />
                        </center>
                        <h2>  หมวกแคมป์ปิ้ง </h2>
                        <Button variant="contained" href="/buy" size="large" style={{  fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={cap12} style={{ width: "300px", marginBottom : -80 }} />
                        </center>
                        <h2>  หมวกแก๊ป </h2>
                        <Button variant="contained" href="/buy" size="large" style={{  fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={cap13} style={{ width: "300px", marginBottom : -80 }} />
                        </center>
                        <h2>  หมวกแฟชั่น </h2>
                        <Button variant="contained" href="/buy" size="large" style={{  fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={cap14} style={{ width: "300px", marginBottom : -80 }} />
                        </center>
                        <h2>  หมวกทรงเจ้าพ่อ </h2>
                        <Button variant="contained" href="/buy" size="large" style={{  fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
        </div>
    );
}
export default Cap;