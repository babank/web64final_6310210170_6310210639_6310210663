import check from './images/check.png'
import Button from '@mui/material/Button'
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Finish() {
    return (
        <div className="finish" style={{ backgroundColor: '#F9E8CD' }}>
            <br />
            <div>
                <Box sx={{ mx: 70, my: 1 }}>
                    <Paper elevation={20} >
                        <br />
                        <h1> &nbsp;&nbsp; เราได้รับคำสั่งจองของคุณแล้ว </h1>
                        <img src={check} style={{ width: "250px" }} />
                        <h2> &nbsp;&nbsp; ขอบคุณที่สั่งจองสินค้ากับเรา </h2>
                        <h3> &nbsp;&nbsp; รายละเอียดสินค้าจะถูกส่งไปให้ใน SMS Messenger ของคุณ </h3><br />
                    </Paper>
                </Box>
                <br />
                <Button variant="contained" href="/home" size="large" style={{ marginTop: "-1px", fontWeight: "bold" }}> กลับหน้าแรก </Button>
            </div>
            <br /><br />
        </div>
    );
}

export default Finish;