import shoes11 from './images/shoes11.png'
import shoes12 from './images/shoes12.png'
import shoes13 from './images/shoes13.png'
import shoes14 from './images/shoes14.png'

import Button from '@mui/material/Button'
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Shoes() {
    return (
        <div style={{ backgroundColor: '#F9E8CD' }}>
            <br />
            <Box sx={{ mx: 100, my: 0 }}>
                <Paper elevation={20} >
                    <h1 style={{ marginTop: "10px" }}>รองเท้าวินเทจ</h1>
                </Paper>
            </Box>

            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={shoes11} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  รองเท้าหุ้มส้น </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={shoes12} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  รองเท้าผ้าใบ </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={shoes13} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  รองเท้าหนัง </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={shoes14} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  รองเท้าแตะ </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
        </div>
    );
}

export default Shoes;