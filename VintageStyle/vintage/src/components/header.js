import { Link } from "react-router-dom";

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>

          <Typography variant="h6">
            VINTAGE STYLE &nbsp;:
          </Typography>

          &nbsp; &nbsp;

          <Link to="/home">
            <Typography variant="body1" sx={{ marginLeft: "20px" }}>
              หน้าแรก
            </Typography>
          </Link>

          &nbsp; &nbsp; &nbsp;
          <Link to="/product">
            <Typography variant="body1">
              ชนิดสินค้า
            </Typography>
          </Link>

          &nbsp; &nbsp; &nbsp;
          <Link to="/buy">
            <Typography variant="body1">
              การสั่งจอง
            </Typography>
          </Link>

          &nbsp; &nbsp; &nbsp;
          <Link to="/signup">
            <Typography variant="body1">
              ลงทะเบียน
            </Typography>
          </Link>

          &nbsp; &nbsp; &nbsp;
          <Link to="/login">
            <Typography variant="body1">
              เข้าสู่ระบบ
            </Typography>
          </Link>

        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;
