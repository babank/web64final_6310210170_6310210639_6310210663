import bag11 from './images/bag11.png'
import bag12 from './images/bag12.png'
import bag13 from './images/bag13.png'
import bag14 from './images/bag14.png'
import bag15 from './images/bag15.png'

import Button from '@mui/material/Button'
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Bag() {
    return (
        <div style={{ backgroundColor: '#F9E8CD' }}>
            <br />
            <Box sx={{ mx: 100, my: 0 }}>
                <Paper elevation={20} >
                    <h1 style={{ marginTop: "10px" }}>กระเป๋าวินเทจ</h1>
                </Paper>
            </Box>

            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={bag11} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กระเป๋าตังแบบหนัง </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={bag12} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กระเป๋าสะพายข้าง </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={bag13} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กระเป๋าสะพายข้างแบบหนัง </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={bag14} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กระเป๋าตังวินเทจ </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={bag15} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กระเป๋าตังแฟชั่น </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
                <br />
            </div>
        </div>
    );
}
export default Bag;