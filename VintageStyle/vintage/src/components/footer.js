import './footer.css';

function Footer() {

    return (

        <footer className="footer" style={{ backgroundColor: '#ff9800' }}>

            <div className="float-left">
                <h2> VINTAGE STYLE </h2>
                <h3> <img src="https://img.icons8.com/color/48/000000/marker--v1.png" style={{ width: "40px",marginBottom: -27 }} />  10 หมู่6 ซ.1/1 ถ.ทุ่งรี ต.คอหงส์
                    <br /> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;อ.หาดใหญ่ จ.สงขลา 90110 </h3>
                <h3> Tel : 0888888888 ☎️ </h3>
            </div>

            <div className="float-right">
                <h2> ติดต่อเรา </h2>
                <h3> <img src="https://img.icons8.com/fluency/48/000000/facebook-new.png" style={{ width: "20px",marginBottom: -3 }} /> Facebook : VINTAGE STYLE
                    <br /> <img src="https://img.icons8.com/color/48/000000/line-me.png" style={{ width: "20px",marginBottom: -4 }} /> Line : @VINTAGESTYLE
                    <br /> <img src="https://img.icons8.com/fluency/48/000000/instagram-new.png" style={{ width: "20px",marginBottom: -4 }} />Instagram : VINTAGE_STYLE</h3>
            </div>
            <br /><br /><br /><br /><br /><br /><br />

        </footer>
    );
}

export default Footer;