import pant11 from './images/pant11.png'
import pant12 from './images/pant12.png'
import pant13 from './images/pant13.png'
import pant14 from './images/pant14.png'
import pant15 from './images/pant15.png'

import Button from '@mui/material/Button'
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Pant() {
    return (
        <div style={{ backgroundColor: '#F9E8CD' }}>
            <br />
            <Box sx={{ mx: 100, my: 0 }}>
                <Paper elevation={20} >
                    <h1 style={{ marginTop: "10px" }}>กางเกงวินเทจ</h1>
                </Paper>
            </Box>

            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={pant11} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กางเกงขาสั้น </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={pant12} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กางเกงวินเทจ </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={pant13} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กางเกงแคมป์ปิ้ง </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={pant14} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กางเกงขายาว</h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={pant15} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  กางเกงแฟชั่น </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
                <br />
            </div>
        </div>
    );
}

export default Pant;