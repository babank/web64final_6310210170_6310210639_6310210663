import tshirt11 from './images/tshirt11.png'
import tshirt12 from './images/tshirt12.png'
import tshirt13 from './images/tshirt13.png'
import tshirt14 from './images/tshirt14.png'
import tshirt15 from './images/tshirt15.png'

import Button from '@mui/material/Button'
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Tshirt() {
    return (
        <div style={{ backgroundColor: '#F9E8CD' }}>
            <br />
            <Box sx={{ mx: 100, my: 0 }}>
                <Paper elevation={20} >
                    <h1 style={{ marginTop: "10px" }}>เสื้อวินเทจ</h1>
                </Paper>
            </Box>

            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={tshirt11} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  เสื้อวินเทจ </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={tshirt12} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  เสื้อแขนยาววินเทจ </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={tshirt13} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  เสื้อวินเทจมือ 2 </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={tshirt14} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  เสื้อแฟชั่น </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
            </div>
            <br />
            <div>
                <Box sx={{ mx: 90, my: 1 }}>
                    <Paper elevation={20} >
                        <center>
                            <img src={tshirt15} style={{ width: "300px", marginBottom: -80 }} />
                        </center>
                        <h2>  เสื้อแขนยาวแฟชั่น </h2>
                        <Button variant="contained" href="/buy" size="large" style={{ fontWeight: "bold", marginBottom: 50 }}> สั่งจอง </Button>
                    </Paper>
                </Box>
                <br />
            </div>
        </div>
    );
}
export default Tshirt;